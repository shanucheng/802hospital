//
//  ItemDetailViewController.m
//  802Hospital
//
//  Created by chenghsienyu on 27/04/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import "ItemDetailViewController.h"
#import "SVProgressHUD.h"
#import "RXMLElement.h"

@interface ItemDetailViewController ()<NSXMLParserDelegate>
{
    NSXMLParser *xmlParser;
    NSMutableString *cdata;
    NSString *element;

}
@end

@implementation ItemDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //NSLog(@"item_id = %@", self.item_id);
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMdd"];
    NSDateFormatter *yearFormatter = [[NSDateFormatter alloc] init];
    [yearFormatter setDateFormat:@"YYYY"];
    NSString *currentDate = [dateFormatter stringFromDate:date];
    NSString *currentYear = [yearFormatter stringFromDate:date];
    NSString *currentTime = [NSString stringWithFormat:@"%ld%@", currentYear.integerValue - 1911, currentDate];
    
    //NSLog(@"currentTime = %@", currentTime);
    
    NSString *soapMessage = [NSString stringWithFormat:
                             @"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                             "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">\n"
                             "<soapenv:Header/>\n"
                             "<soapenv:Body>\n"
                             "<tem:ScheduleByDivision>\n"
                             "<tem:stLookDt>%@</tem:stLookDt>\n"
                             "<tem:stSectno>%@</tem:stSectno>\n"
                             "<tem:sLoginHospital>%@</tem:sLoginHospital>\n"
                             "</tem:ScheduleByDivision>\n"
                             "</soapenv:Body>\n"
                             "</soapenv:Envelope>\n", currentTime, self.item_id, @"0502080015"
                             ];
    //NSLog(@"soapMSG = %@", soapMessage);
    
    NSURL *url = [NSURL URLWithString:@"http://61.218.227.166:8081/TREService.svc?wsdl"];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: @"http://tempuri.org/ITREService/ScheduleByDivision" forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    [SVProgressHUD show];
    
    [NSURLConnection sendAsynchronousRequest:theRequest
     // the NSOperationQueue upon which the handler block will be dispatched:
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               // back on the main thread, check for errors, if no errors start the parsing
                               //
                               [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                               
                               // here we check for any returned NSError from the server, "and" we also check for any http response errors
                               if (error != nil) {
                                   [self handleError:error];
                               }
                               else {
                                   // check for any response errors
                                   NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                   if ((([httpResponse statusCode]/100) == 2) && [[response MIMEType] isEqual:@"text/xml"]) {
                                       
                                       // Update the UI and start parsing the data,
                                       // Spawn an NSOperation to parse the earthquake data so that the UI is not
                                       // blocked while the application parses the XML data.
                                       xmlParser = [[NSXMLParser alloc] initWithData:data];
                                       [xmlParser setDelegate:self];
                                       [xmlParser setShouldResolveExternalEntities:NO];
                                       [xmlParser parse];
                                       
                                   }
                                   else {
                                       NSString *errorString =
                                       NSLocalizedString(@"HTTP Error", @"Error message displayed when receving a connection error.");
                                       NSDictionary *userInfo = @{NSLocalizedDescriptionKey : errorString};
                                       NSError *reportError = [NSError errorWithDomain:@"HTTP"
                                                                                  code:[httpResponse statusCode]
                                                                              userInfo:userInfo];
                                       [self handleError:reportError];
                                   }
                               }
                           }];

}

- (void)handleError:(NSError *)error {
    
    NSString *errorMessage = [error localizedDescription];
    NSString *alertTitle = NSLocalizedString(@"Error", @"Title for alert displayed when download or parse error occurs.");
    NSString *okTitle = NSLocalizedString(@"OK ", @"OK Title for alert displayed when download or parse error occurs.");
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alertTitle message:errorMessage delegate:nil cancelButtonTitle:okTitle otherButtonTitles:nil];
    [alertView show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark xmlparser delegate

- (void) parserDidStartDocument:(NSXMLParser *)parser {
    //NSLog(@"parserDidStartDocument");
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    //NSLog(@"didStartElement --> %@", elementName);
    element = elementName;
    if ([element isEqualToString:@"ScheduleByDivisionResult"]) {
        cdata = [[NSMutableString alloc] init];
    }

}

-(void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    //NSLog(@"foundCharacters --> %@", string);
    if ([element containsString:@"ScheduleByDivisionResult"]) {
        [cdata appendString:string];
    }

}



- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    //NSLog(@"didEndElement   --> %@", elementName);
    if ([elementName isEqualToString:@"ScheduleByDivisionResult"]) {
        //NSLog(@"cdata ---> %@", cdata);
        NSData *data = [cdata dataUsingEncoding:NSUTF8StringEncoding];
        RXMLElement *xml = [RXMLElement elementFromXMLData:data];
        RXMLElement *xmlDt = [xml child:@"Dt"];
        RXMLElement *xmlShift = [xmlDt child:@"Shift"];
        NSLog(@"lookdt = %@", [xmlDt attribute:@"lookdt"]);
        NSLog(@"ShiftID = %@", [xmlShift attribute:@"id"]);
        NSLog(@"ShifttraNam = %@", [xmlShift attribute:@"traNam"]);
        
        
        
        
    }

    }

- (void) parserDidEndDocument:(NSXMLParser *)parser {
    NSLog(@"parserDidEndDocument");
    [SVProgressHUD dismiss];
}

- (void)parser:(NSXMLParser *)parser foundCDATA:(NSData *)CDATABlock{
    NSString *someString = [[NSString alloc] initWithData:CDATABlock encoding:NSUTF8StringEncoding];
    NSLog(@"cData:%@",someString);
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - UITableView Delegates

- (NSInteger)numberOfSectionsInTableView: (UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}


@end
