//
//  BookingViewController.h
//  802Hospital
//
//  Created by chenghsienyu on 19/03/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
