//
//  BookingViewController.m
//  802Hospital
//
//  Created by chenghsienyu on 19/03/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import "BookingViewController.h"
#import "SVProgressHUD.h"
#import "ItemDetailViewController.h"

@interface BookingViewController ()<NSXMLParserDelegate>
{
    NSXMLParser *xmlParser;
    NSMutableArray *items;
    NSMutableDictionary *itemDict;
    NSMutableString *cdata;
    NSString *element;
}


@end

@implementation BookingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"行動掛號";
    
    // Do any additional setup after loading the view.
    items = [[NSMutableArray alloc] init];
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMdd"];
    NSDateFormatter *yearFormatter = [[NSDateFormatter alloc] init];
    [yearFormatter setDateFormat:@"YYYY"];
    NSString *currentDate = [dateFormatter stringFromDate:date];
    NSString *currentYear = [yearFormatter stringFromDate:date];
    NSString *currentTime = [NSString stringWithFormat:@"%ld%@", currentYear.integerValue - 1911, currentDate];
    
    //NSLog(@"currentTime = %@", currentTime);
    
    NSString *soapMessage = [NSString stringWithFormat:
                             @"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                             "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">\n"
                             "<soapenv:Header/>\n"
                             "<soapenv:Body>\n"
                             "<tem:GetDivisions>\n"
                             "<tem:stLookDt>%@</tem:stLookDt>\n"
                             "<tem:sLoginHospital>%@</tem:sLoginHospital>\n"
                             "</tem:GetDivisions>\n"
                             "</soapenv:Body>\n"
                             "</soapenv:Envelope>\n", currentTime, @"0502080015"
                             ];
    //NSLog(@"soapMSG = %@", soapMessage);
    
    NSURL *url = [NSURL URLWithString:@"http://61.218.227.166:8081/TREService.svc?wsdl"];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: @"http://tempuri.org/ITREService/GetDivisions" forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    [SVProgressHUD show];

    [NSURLConnection sendAsynchronousRequest:theRequest
     // the NSOperationQueue upon which the handler block will be dispatched:
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               // back on the main thread, check for errors, if no errors start the parsing
                               //
                               [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                               
                               // here we check for any returned NSError from the server, "and" we also check for any http response errors
                               if (error != nil) {
                                   [self handleError:error];
                               }
                               else {
                                   // check for any response errors
                                   NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                   if ((([httpResponse statusCode]/100) == 2) && [[response MIMEType] isEqual:@"text/xml"]) {
                                       
                                       // Update the UI and start parsing the data,
                                       // Spawn an NSOperation to parse the earthquake data so that the UI is not
                                       // blocked while the application parses the XML data.
                                       xmlParser = [[NSXMLParser alloc] initWithData:data];
                                       [xmlParser setDelegate:self];
                                       [xmlParser setShouldResolveExternalEntities:NO];
                                       [xmlParser parse];

                                   }
                                   else {
                                       NSString *errorString =
                                       NSLocalizedString(@"HTTP Error", @"Error message displayed when receving a connection error.");
                                       NSDictionary *userInfo = @{NSLocalizedDescriptionKey : errorString};
                                       NSError *reportError = [NSError errorWithDomain:@"HTTP"
                                                                                  code:[httpResponse statusCode]
                                                                              userInfo:userInfo];
                                       [self handleError:reportError];
                                   }
                               }
                           }];

    
}

- (void)handleError:(NSError *)error {
    
    NSString *errorMessage = [error localizedDescription];
    NSString *alertTitle = NSLocalizedString(@"Error", @"Title for alert displayed when download or parse error occurs.");
    NSString *okTitle = NSLocalizedString(@"OK ", @"OK Title for alert displayed when download or parse error occurs.");
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alertTitle message:errorMessage delegate:nil cancelButtonTitle:okTitle otherButtonTitles:nil];
    [alertView show];
}


#pragma mark xmlparser delegate

- (void) parserDidStartDocument:(NSXMLParser *)parser {
    //NSLog(@"parserDidStartDocument");
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    //NSLog(@"didStartElement --> %@", elementName);
    element = elementName;
    if ([element isEqualToString:@"GetDivisionsResult"]) {
        cdata = [[NSMutableString alloc] init];
        itemDict = [[NSMutableDictionary alloc] init];
    }
}

-(void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    //NSLog(@"foundCharacters --> %@", string);
    if ([element containsString:@"GetDivisionsResult"]) {
        [cdata appendString:string];
    }
}



- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    //NSLog(@"didEndElement   --> %@", elementName);
    if ([elementName isEqualToString:@"GetDivisionsResult"]) {
        //NSLog(@"cdata ---> %@", cdata);
        NSArray *tempArray = [cdata componentsSeparatedByString:@"\n"];
        //NSLog(@"tempArray = %@", tempArray);
        for (int i = 0; i<tempArray.count; i++) {
            NSString *tempString = [tempArray objectAtIndex:i];
            //NSLog(@"temp = %@", tempString);
            
            NSString *pattern = @".*?(\".*?\").*?(\".*?\").*?(\".*?\")";
            
            NSRegularExpression *regex = [NSRegularExpression
                                          regularExpressionWithPattern:pattern
                                          options:0
                                          error:nil];
            NSArray *matches = [regex matchesInString:tempString options:0 range: NSMakeRange(0, tempString.length)];
            for (NSTextCheckingResult* match in matches) {
                //NSString* matchText = [tempString substringWithRange:[match range]];
                //NSLog(@"match: %@", matchText);
                NSRange temp_id = [match rangeAtIndex:1];
                NSRange temp_traNam= [match rangeAtIndex:2];
                NSRange temp_engNam = [match rangeAtIndex:3];
                //NSLog(@"group1: %@", [tempString substringWithRange:item_id]);
                //NSLog(@"group2: %@", [tempString substringWithRange:item_traNam]);
                //NSLog(@"group3: %@", [tempString substringWithRange:item_engNam]);
                NSString *item_id = [[tempString substringWithRange:temp_id] substringWithRange:NSMakeRange(1, [[tempString substringWithRange:temp_id] length] -2)];
                NSString *item_traName = [[tempString substringWithRange:temp_traNam] substringWithRange:NSMakeRange(1, [[tempString substringWithRange:temp_traNam] length] -2)];
                NSString *item_engName = [[tempString substringWithRange:temp_engNam] substringWithRange:NSMakeRange(1, [[tempString substringWithRange:temp_engNam] length] -2)];
                
                [itemDict setObject:item_id forKey:@"id"];
                [itemDict setObject:item_traName forKey:@"traName"];
                [itemDict setObject:item_engName forKey:@"engName"];
                [items addObject:[itemDict copy]];
            }
        }
        //NSLog(@"items = %@", items);
    }
    
}

- (void) parserDidEndDocument:(NSXMLParser *)parser {
    //NSLog(@"parserDidEndDocument");
    [SVProgressHUD dismiss];
    [self.tableView reloadData];
}

- (void)parser:(NSXMLParser *)parser foundCDATA:(NSData *)CDATABlock{
    NSLog(@"cData:%@",[NSString stringWithUTF8String:[CDATABlock bytes]]);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Assume self.view is the table view
    NSIndexPath *path = [self.tableView indexPathForSelectedRow];
    ItemDetailViewController *vc = (ItemDetailViewController *)segue.destinationViewController;
    vc.item_id = [[items objectAtIndex:path.row] valueForKey:@"id"];
}

#pragma mark - UITableView Delegates

- (NSInteger)numberOfSectionsInTableView: (UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [items count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        
    }
    cell.textLabel.textColor = [UIColor blueColor];
    cell.textLabel.text = [[items objectAtIndex:indexPath.row] valueForKey:@"traName"];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //NSLog(@"Row Selected = %li",(long)indexPath.row);
    
    [self performSegueWithIdentifier:@"toItemDetail" sender:self.view];
    
    
}




@end
