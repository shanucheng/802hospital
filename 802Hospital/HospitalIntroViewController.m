//
//  HospitalIntroViewController.m
//  802Hospital
//
//  Created by chenghsienyu on 18/03/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import "HospitalIntroViewController.h"

@interface HospitalIntroViewController ()

@end

@implementation HospitalIntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"醫院簡介";
    [self contentSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)contentSetup {
    
    self.scrollView.contentSize = CGSizeMake(self.background.frame.size.width, self.background.frame.size.height *2);
}

@end
