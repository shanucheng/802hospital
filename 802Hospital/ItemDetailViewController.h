//
//  ItemDetailViewController.h
//  802Hospital
//
//  Created by chenghsienyu on 27/04/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemDetailViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSString *item_id;

@end
