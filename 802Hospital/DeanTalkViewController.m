//
//  DeanTalkViewController.m
//  802Hospital
//
//  Created by chenghsienyu on 18/03/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import "DeanTalkViewController.h"

@interface DeanTalkViewController ()

@end

@implementation DeanTalkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"院長的話";

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
