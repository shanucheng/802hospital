//
//  DoctorMenuViewController.h
//  802Hospital
//
//  Created by chenghsienyu on 18/03/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoctorMenuViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
