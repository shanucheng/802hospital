//
//  HospitalLocationViewController.m
//  802Hospital
//
//  Created by chenghsienyu on 18/03/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import "HospitalLocationViewController.h"

@interface HospitalLocationViewController ()

@end

@implementation HospitalLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"交通位置";

    NSURL *url = [NSURL URLWithString:@"http://818h.mnd.gov.tw/btWeb/usrHtml/btcar.htm"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
